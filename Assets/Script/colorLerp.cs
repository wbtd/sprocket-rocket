﻿using UnityEngine;
using System.Collections;

public class colorLerp : MonoBehaviour {
	[SerializeField] private Color colour1;
	[SerializeField] private Color colour2;
	[SerializeField] private float speed;
	private SpriteRenderer sprite;
	// Use this for initialization
	void Start () {
		sprite = GetComponent <SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		sprite.color = Color.Lerp (colour1, colour2, Mathf.Abs(Mathf.Sin (Time.timeSinceLevelLoad * speed)));
	}
}
