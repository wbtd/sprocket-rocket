﻿using UnityEngine;
using System.Collections;

public class randomiseProperties : MonoBehaviour {
	[SerializeField] private Color colour1 = Color.gray;
	[SerializeField] private Color colour2 = Color.black;
	[SerializeField] private float scale = 0f;
	[SerializeField] private float rotation = 0f;
	[SerializeField] private bool massVariance = false;
	// Use this for initialization
	void Start () {
		Color colorisMundi = Color.Lerp (colour1, colour2, Random.Range (0f,1f));
		SpriteRenderer sprite = GetComponent <SpriteRenderer>();
		sprite.color = colorisMundi;
		transform.rotation = Quaternion.Euler(transform.rotation.x,transform.rotation.y, transform.rotation.z + Random.Range (-rotation, rotation));
		transform.localScale = transform.localScale.normalized * (transform.localScale.magnitude + Random.Range (-scale, scale));
		if (GetComponent<Gravity>() && massVariance)
		{
			Gravity gravity = GetComponent<Gravity>();
			gravity.SetFM(gravity.GetFM() * transform.localScale.magnitude);
		}
	}
}
