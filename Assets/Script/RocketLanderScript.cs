﻿using UnityEngine;
using System.Collections;

public class RocketLanderScript : MonoBehaviour {
	[SerializeField] private Rigidbody2D rb;
	[SerializeField] private float power = 2f;
	//[SerializeField] private Transform planet;
	[SerializeField] private ParticleSystem[] engineEffect;
	[SerializeField] private float maxSpeed = 5;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		engineEffect = GetComponentsInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
	if (Input.GetButton("Fire1"))
		{
			Power ();
			foreach (ParticleSystem e in engineEffect){
				e.startLifetime = 0.6f;
			}

		}
	else if (Input.GetButtonUp("Fire1")){
			foreach (ParticleSystem e in engineEffect){
				e.startLifetime = 0f;
			}
		}
	if (Input.acceleration.x != 0)
			rb.AddTorque(Input.acceleration.x * -2f);
	if (rb.velocity.magnitude > maxSpeed)
			rb.velocity = rb.velocity.normalized * maxSpeed;
	}
	void Power ()
	{
		rb.AddForce(transform.up.normalized * power);
	}
	void FixedUpdate () 
	{
		rb.rotation = Vector2.Angle(Vector2.up, rb.velocity.normalized);
		Debug.DrawLine(rb.position, (rb.position + rb.velocity),Color.magenta);
	}
}
