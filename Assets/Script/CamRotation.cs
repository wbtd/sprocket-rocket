﻿using UnityEngine;
using System.Collections;

public class CamRotation : MonoBehaviour {
	[SerializeField]	GameObject planet;
	[SerializeField]	GameObject rocket;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float z = Vector2.Angle(Vector2.up, (rocket.transform.position -planet.transform.position));
		z = z * Mathf.Sign (planet.transform.position.x - rocket.transform.position.x);
		transform.rotation =  Quaternion.Euler(0,0,z);
	}
}
