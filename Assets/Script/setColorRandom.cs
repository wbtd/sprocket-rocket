﻿using UnityEngine;
using System.Collections;

public class setColorRandom : MonoBehaviour {
	[SerializeField] private Color colour1;
	[SerializeField] private Color colour2;
	// Use this for initialization
	void Start () {
		GetComponent <SpriteRenderer>().color = Color.Lerp (colour1, colour2, Random.Range (0,1));
	}
}
