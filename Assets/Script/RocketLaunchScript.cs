﻿using UnityEngine;
using System.Collections;

public class RocketLaunchScript : MonoBehaviour {

	[Tooltip("The maximum power of the engines.")]
	[SerializeField] private float MaxPower = 2f;
	[Tooltip("The launch angle, + and - from 0.")]
	[SerializeField] private float MaxLaunchAngleVariation = 45f;
	[Tooltip("Particle systems for engines. Leave blank to find automatically from child game objects.")]
	[SerializeField] private ParticleSystem[] engineEffect;
	[Tooltip("The maximum velocity of the craft. Note, changing this affects the gameplay" +
		" as gravity will have a weaker effect at higher speeds. Remember to tweak the GUI Tuning " +
		"property on all gravity objects to reflect this. (rule of thumb: GUITweak = this + 3)")]
	[SerializeField] private float maxSpeed = 5;
	[SerializeField] private GameObject explosion;
	private Rigidbody2D rb;
	private int HUnit;
	private int WUnit;
	private enum State {launch, flying};
	private State state = State.launch;
	private enum SwipeState {horizontal, vertical, virgin, screenSlide};
	private SwipeState swipeState = SwipeState.virgin;
	private float launchAngle = 0f;
	private float power;
	private Camera cam;
	private float origiSize;
	private float viewSlide = 0f;
	private bool powering = true;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		if(engineEffect.Length == 0) engineEffect = GetComponentsInChildren<ParticleSystem>();
		HUnit = Screen.height / 10;
		WUnit = Screen.width / 10;
		cam = Camera.main;
		origiSize = cam.orthographicSize;
	}
	
	// Update is called once per frame
	void Update () {
	if (rb.velocity.magnitude > maxSpeed)
			rb.velocity = rb.velocity.normalized * maxSpeed;

		if (Input.touchCount > 0)
		{
			//viewslider
			if (Input.touches[0].position.y > 4*HUnit) swipeState = SwipeState.screenSlide;
			if (swipeState == SwipeState.screenSlide)
			{
				viewSlide += (Input.touches[0].deltaPosition.x / Screen.width);
				if (Input.touches[0].phase == TouchPhase.Ended)
				{
					if (viewSlide >= 0.5f)
						viewSlide = 1f;
					else
						viewSlide = 0f;
					swipeState = SwipeState.virgin;
				}
				cam.rect = new Rect(viewSlide,0f,Screen.width,Screen.height);
			}
			//end viewslider

			//rocketControl
			if (state == State.launch)
			{
				if (Input.touches[0].position.y <= 4*HUnit)
				{
					Vector2 td = Input.touches[0].deltaPosition;
					if (swipeState == SwipeState.virgin && Input.touches[0].phase == TouchPhase.Moved)			//use a swipsestate to determine the nature of the swipe
					{															//a virgin ss means it has no state
						if (td.x*td.x > td.y*td.y) swipeState = SwipeState.horizontal;	//if x > y in the swipe, it is horizontal. lock it
						else if (td.x*td.x < td.y*td.y)swipeState = SwipeState.vertical;					//else it is vertical. lock it.
					}
					if (swipeState == SwipeState.horizontal)					//do horizontal stuff
					{
						launchAngle += (td.x / 10f);
						if (launchAngle > MaxLaunchAngleVariation)
							launchAngle = MaxLaunchAngleVariation;
						else if (launchAngle < -MaxLaunchAngleVariation)
							launchAngle = -MaxLaunchAngleVariation;

						if(Input.touches[0].phase == TouchPhase.Ended)
							swipeState = SwipeState.virgin;						//return virginity when we lift fingers off
					}
					else if (swipeState == SwipeState.vertical)					//power part
					{
						power += td.y/(MaxPower*10f);
						if (power > MaxPower) power = MaxPower;
						if (power < 0f) power = 0f;

						if(Input.touches[0].phase == TouchPhase.Ended)
						{
							if (power > 0f)										//launch if power is applied. cancel if 0, so player can readjust.
							{
								Power ();
								powerOff(power);
								state = State.flying;
							}
							swipeState = SwipeState.virgin;						//return virginity when we lift fingers off
						}
					}
				}
			}
		}
	}
	void powerOff (float seconds)
	{
		Invoke("powerOffCR", seconds);
	}
	void powerOffCR ()
	{
		powering = false;
	}
	void Cam ()
	{
		float camDistance = rb.velocity.magnitude;
		if (camDistance > cam.orthographicSize)
			camDistance = cam.orthographicSize;
		Vector3 pos = rb.position + rb.velocity.normalized * camDistance;
		cam.orthographicSize = origiSize + (camDistance * 1.5f);
		pos.z = -50;
		cam.transform.position  =  pos;
	}
	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.collider.tag != "goal")
			crash ();
	}
	void crash ()
	{
		rb.velocity = new Vector2(0f,0f);
		Instantiate(explosion,transform.position,Quaternion.identity);
	}
	void land ()
	{

	}
	void Power ()
	{
		rb.AddForce(transform.up.normalized * MaxPower);
		foreach (ParticleSystem ef in engineEffect)
		{
			ef.startLifetime = power;
		}
	}
	void FixedUpdate () 
	{
		if (state == State.flying && rb.velocity.magnitude > 0f){
			float angle = Vector2.Angle(Vector2.up, rb.velocity);
			angle = angle * Mathf.Sign (rb.position.x - (rb.position + rb.velocity).x);
			rb.rotation = angle;
			Debug.DrawLine(rb.position, (rb.position + rb.velocity),Color.magenta);
			if (powering) Power();
		}
		else if (state == State.launch){
			rb.rotation = launchAngle;
		}
		Cam();
	}
	void OnGUI ()
	{
		if (state == State.launch)
		{
			launchAngle = GUI.HorizontalSlider(new Rect(WUnit * 4, HUnit * 9, WUnit * 3, HUnit * 1), launchAngle, MaxLaunchAngleVariation, -MaxLaunchAngleVariation);
			power = GUI.VerticalSlider(new Rect(WUnit * 9, HUnit * 7, WUnit * 1, WUnit * 3), power, MaxPower, 0);
			if (GUI.Button(new Rect (WUnit, HUnit * 9, WUnit * 2, 20),"Ignite!"))
			{
				Power ();
				powerOff(power);
				state = State.flying;
			}
		}
	}
}
