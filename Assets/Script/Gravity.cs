﻿using UnityEngine;
using System.Collections;


public class Gravity : MonoBehaviour
{
	[Tooltip("The radius of effect. Changing this value has no effect on the strength or falloff, think of it as a cutoff.")]
	[SerializeField] private float radius = 5f;
	[Tooltip("Used to multiply gravitational strength. If no rigidbody2D attached, a default mass of 100 is assumed.")]
	[SerializeField] private float forceMultiplier = 1f;
	[Tooltip("Used to tweak the scale of the gravity well when changing rocket mass/max speed.")]
	[SerializeField] private float GUITuning = 5f;
	private float force = 0f;
	private float mass;

	void Start ()
	{
		if (GetComponent<Rigidbody2D>())
			mass = GetComponent<Rigidbody2D>().mass;
		else
			mass = 100f;
	}
	public float GetFM ()
	{
		return forceMultiplier;
	}
	public void SetFM (float amount)
	{
		forceMultiplier = amount;
	}
	void Update() 
	{

		RaycastHit2D[] nearObjects = Physics2D.CircleCastAll(transform.position, radius, Vector2.up);
		if (nearObjects != null)
		foreach (RaycastHit2D nObject in nearObjects)
		{
			if (nObject.collider.GetComponent<Rigidbody2D>() && !isSelf (nObject.collider))
			{
				nObject.collider.GetComponent<Rigidbody2D>().AddForce ((transform.position - nObject.transform.position).normalized * CalculateForce(nObject.transform));
			}
		}
	}

	bool isSelf (Collider2D nObject)
	{
		bool hitSelf = false;

		if (nObject == GetComponent<Collider2D>())
			return true;

		foreach (Collider2D col in GetComponentsInChildren<Collider2D>())
		{
			if (col == nObject) return true;
		}
		return hitSelf;
	}

	float CalculateForce(Transform nObject)
	{
		float m = mass;
		float d = (nObject.position - transform.position).magnitude;
		float g = forceMultiplier * m / (d*d);
		if (g < 0) g = 0;
		return g;
	}
	void OnDrawGizmos ()
	{

		Gizmos.color = Color.green;

		float m = 100f;
		if (GetComponent<Rigidbody2D>())
			m = GetComponent<Rigidbody2D>().mass;
		float g = (forceMultiplier * m)/GUITuning;

		for (int i = 2; i < 40; ++i)
		{
			float radius2 = Mathf.Sqrt(g*(g/i));
			if (radius2 > radius) radius2 = radius;
			Gizmos.color = Color.Lerp (Color.green,Color.red, (float) i/40);
			Gizmos.DrawWireSphere(transform.position, radius2);
		}

		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}